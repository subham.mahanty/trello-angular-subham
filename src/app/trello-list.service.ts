import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IList } from './list';
import { Observable } from 'rxjs';

const key = 'd6d1ba11fed745fd761418b33e64d340';
const token = '195c606b7f9ee2ea6172e10277451e19943b2969612d1584d4c020a4817a8104';
const id = '4gXOcM4H';
const idBoard = '5d19a4d2b57b1159f8100df6';


@Injectable({
  providedIn: 'root'
})
export class TrelloListService {
  
  private _urlList: string = `https://trello.com/1/boards/${id}/lists?key=${key}&token=${token}`;

  constructor(private http: HttpClient) {}

  getList(): Observable<IList[]>{
    return this.http.get<IList[]>(this._urlList);
  }

  postList(newListName): Observable<IList>{
    let _urlPostList = `https://api.trello.com/1/lists?name=${newListName}&idBoard=${idBoard}&pos=bottom&key=${key}&token=${token}`;

    return this.http.post<IList>(_urlPostList, null);
  }
}