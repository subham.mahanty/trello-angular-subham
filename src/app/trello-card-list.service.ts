import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICard } from './card';

const key = 'd6d1ba11fed745fd761418b33e64d340';
const token = '195c606b7f9ee2ea6172e10277451e19943b2969612d1584d4c020a4817a8104';

@Injectable({
  providedIn: 'root'
})
export class TrelloCardListService {

  constructor(private http: HttpClient ) {}
  
  getCardList(cardId) {
    let _urlCardList = `https://api.trello.com/1/lists/${cardId}/cards?key=${key}&token=${token}`;

    return this.http.get<ICard[]>(_urlCardList);
  }

  postCardList(idList, cardName): Observable<ICard>{
    let _urlCardList = `https://api.trello.com/1/cards?idList=${idList}&keepFromSource=all&key=${key}&token=${token}&name=${cardName}`;

    return this.http.post<ICard>(_urlCardList, null);
  }

  deleteCardList(cardId):Observable<ICard>{
    let _urlCardList = `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`;
    
    return this.http.delete<ICard>(_urlCardList);
  }

  putCardList(cardId, cardName): Observable<ICard>{
    let _urlCardList = `https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}&name=${cardName}`;
    console.log(cardId);
    console.log(cardName);
    
    return this.http.put<ICard>(_urlCardList, null);
  }
}