import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrelloListsComponent } from './trello-lists/trello-lists.component';
import { CheckListsComponent } from './check-lists/check-lists.component';

// Here name inside {} braces should be same as the name of the class in the .ts file of the perticular component.


const routes: Routes = [
  
  { path: '',redirectTo:'lists', pathMatch: 'full' },
  { path: 'lists', component: TrelloListsComponent }, 
  { path: 'checklist/:cardId' , component: CheckListsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
