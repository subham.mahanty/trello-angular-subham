export interface ICheckItem {
    id: string;
    name: string;
    state: string;
}