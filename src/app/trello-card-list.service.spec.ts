import { TestBed } from '@angular/core/testing';

import { TrelloCardListService } from './trello-card-list.service';

describe('TrelloCardListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrelloCardListService = TestBed.get(TrelloCardListService);
    expect(service).toBeTruthy();
  });
});
