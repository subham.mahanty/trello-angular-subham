import { TestBed } from '@angular/core/testing';

import { CheckListService } from './check-list.service';

describe('CheckListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheckListService = TestBed.get(CheckListService);
    expect(service).toBeTruthy();
  });
});
