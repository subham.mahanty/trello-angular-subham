import { TestBed } from '@angular/core/testing';

import { TrelloListService } from './trello-list.service';

describe('TrelloListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrelloListService = TestBed.get(TrelloListService);
    expect(service).toBeTruthy();
  });
});