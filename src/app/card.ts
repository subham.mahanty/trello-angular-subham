export class ICard {
    id: string;
    name: string;
    editable: boolean;
}