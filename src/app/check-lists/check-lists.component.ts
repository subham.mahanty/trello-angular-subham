import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { CheckListService } from '../check-list.service';
import { IChecks } from '../checkLists';
import { ICheckItem } from '../checkItem';

@Component({
  selector: 'app-check-lists',
  templateUrl: './check-lists.component.html',
  styleUrls: ['./check-lists.component.css']
})
export class CheckListsComponent implements OnInit {
  public checkListDetails:IChecks[] = [];
  public cardId = this.route.snapshot.paramMap.get('cardId');
  public newCheckItemName:string= "";
  public newCheckItem:ICheckItem = {id:'', name: '', state: 'incomplete'};
  public presentCheckItemState: string;

  constructor(
    private route: ActivatedRoute,
    private _checkListService: CheckListService
  ) {}

  ngOnInit() {
    this._checkListService.getCardList(this.cardId)
        .subscribe(data => this.checkListDetails = data);
  }

  // checkListId, checkItemName

  inputNewCheckItem(newValue) {
    this.newCheckItemName = newValue;
  }

  postCheckItems(checkListId){
     this._checkListService.postCheckItems(checkListId, this.newCheckItemName)
      .subscribe( data => {
        this.newCheckItem = data;
        this.checkListDetails.map(checklist => {
          if(checklist.id === checkListId){
            checklist.checkItems.push(this.newCheckItem);
          }
        })
      });
  }
  checkedFunction(state){
    return (state==='complete') ? true : false;
  }

  // cardId:string, idCheckItem:string, state:string

  toggleCheckItem(cardId, idCheckItem, state) {
    this._checkListService.toggleCheckItems(cardId, idCheckItem, state)
    .subscribe(() => {
      
    });
  }
}