export interface IChecks {
    id: string;
    name: string;
    checkItems: any;
}

// what is the type of checkItems?