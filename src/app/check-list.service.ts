import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IChecks } from './checkLists';
import { Observable } from 'rxjs';
import { ICheckItem } from './checkItem';

const key = 'd6d1ba11fed745fd761418b33e64d340';
const token = '195c606b7f9ee2ea6172e10277451e19943b2969612d1584d4c020a4817a8104';

@Injectable({
  providedIn: 'root'
})
export class CheckListService {
  constructor(private http: HttpClient) {}

  getCardList(cardId) {
    let getCheckListData = `https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`;
    
    return this.http.get<IChecks[]>(getCheckListData);
  }

  postCheckItems(checkListId: string, checkItemName:string): Observable<ICheckItem> {
    let _url =`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&pos=bottom&checked=false&key=${key}&token=${token}`;
    console.log('servic file')
    return this.http.post<ICheckItem>(_url, null);
  }

  toggleCheckItems(cardId:string, idCheckItem:string, state:string): Observable<ICheckItem>{
    let _urlState = `https://api.trello.com/1/cards/${cardId}/checkItem/${idCheckItem}?${state}&key=${key}&token=${token}`;

    return this.http.put<ICheckItem>(_urlState, null);
  }

}
