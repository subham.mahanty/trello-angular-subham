import { Component, OnInit, Input } from '@angular/core';
import { TrelloCardListService } from '../trello-card-list.service';
import { ICard } from '../card';

@Component({
  selector: 'app-trello-card-list',
  templateUrl: './trello-card-list.component.html',
  styleUrls: ['./trello-card-list.component.css']
})
export class TrelloCardListComponent implements OnInit {

  public cardList: ICard[] = [];
  public currentCard: ICard = {id: '', name: '', editable: false};
  // @Input() public listId: string;  //Getting list id from parent(trello-list.componenet.html)
  public cardName: string;
  @Input() public listId: string;
  public stateVariable = false;
  public cardId:string;

  constructor(private _listService: TrelloCardListService, private _cardService: TrelloCardListService) { }

  ngOnInit() {
    this._listService.getCardList(this.listId)
          .subscribe(data => this.cardList = data);
  }

  postCardList() {
    // this.currentCard.name = this.cardName;
    // this.currentCard.id = "";
    
    this._cardService.postCardList(this.listId, this.cardName)
    .subscribe(data => {
      this.currentCard = data;
      this.cardList.push(this.currentCard);
    });
    
  }

  deleteCardList(cardId: string){
    this._cardService.deleteCardList(cardId)
    .subscribe(() => {
      this.cardList = this.cardList.filter(card => card.id !== cardId);
    });
  }
}