import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrelloCardListComponent } from './trello-card-list.component';

describe('TrelloCardListComponent', () => {
  let component: TrelloCardListComponent;
  let fixture: ComponentFixture<TrelloCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrelloCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrelloCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
