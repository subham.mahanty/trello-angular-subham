import { Component, OnInit, Input } from '@angular/core';
import { TrelloListService } from '../trello-list.service';
import { IList } from '../list';

@Component({
  selector: 'app-trello-lists',
  templateUrl: './trello-lists.component.html',
  styleUrls: ['./trello-lists.component.css']
})
export class TrelloListsComponent implements OnInit {
  public lists = [];
  public listName: IList;
  public currentCard: IList;
  
  constructor(private _listService: TrelloListService) { }

  ngOnInit() {
    this._listService.getList()
        .subscribe(data => {
          this.lists = data;
        });
  }

  postList(){
    this.currentCard = this.listName;
    this._listService.postList(this.listName)
    .subscribe(data => {
      this.currentCard = data;
      this.lists.push(this.currentCard);
    });
  }
}