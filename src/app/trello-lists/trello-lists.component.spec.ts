import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrelloListsComponent } from './trello-lists.component';

describe('TrelloListsComponent', () => {
  let component: TrelloListsComponent;
  let fixture: ComponentFixture<TrelloListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrelloListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrelloListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
