import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrelloListsComponent } from './trello-lists/trello-lists.component';
import { from } from 'rxjs';
import { TrelloListService } from './trello-list.service';
import { TrelloCardListComponent } from './trello-card-list/trello-card-list.component';
import { CheckListsComponent } from './check-lists/check-lists.component'; 
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    TrelloListsComponent,
    TrelloCardListComponent,
    CheckListsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [TrelloListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
